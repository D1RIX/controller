package com.app.Controller;

import com.app.Domain.Client;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//creating RestController
@RestController
public class Controller
{
    //autowired the ClientService class
    @Autowired
    Receiver receiver;

    //autowired the Sender class
    @Autowired
    Sender sender;

    //creating a get mapping that retrieves all the clients detail from the database
    @GetMapping("/client")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    //creating post mapping that post the client detail in the database
    @PostMapping("/client")
    private int saveClient(@RequestBody Client client)
    {
        //sending request
        sender.send(""+
                client.getId()+"-"+
                client.getTrip()+"-"+
                client.getName()+"-"+
                client.getEmail());
        return client.getId();
    }
}